package example.com.melani_1202165279_si40int_pab_modul5;

        import android.support.v7.app.AppCompatActivity;
        import android.os.Bundle;
        import android.widget.ListView;

        import java.util.ArrayList;
        import java.util.List;

public class ListActivity extends AppCompatActivity {

    ListView listViewArtikels;
    List<Article> artikels;
    SharedPref sharedpref;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        sharedpref = new SharedPref(this);
        if(sharedpref.loadNightModeState()==true) {
            setTheme(R.style.darktheme);
        }
        else  setTheme(R.style.AppTheme);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list);
//        listViewArtikels = (ListView) findViewById(R.id.listViewProduks);
        artikels =new ArrayList<>();
    }
}
