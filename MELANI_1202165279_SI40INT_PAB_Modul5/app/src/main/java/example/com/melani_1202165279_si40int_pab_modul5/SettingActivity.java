package example.com.melani_1202165279_si40int_pab_modul5;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.Switch;

public class SettingActivity extends AppCompatActivity {

    private Switch myswitch;
    private Switch myswitch2;
    SharedPref sharedpref;
    Button btnSave;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        sharedpref = new SharedPref(this);
        if(sharedpref.loadNightModeState()== true) {
            setTheme(R.style.darktheme);
        }
        else  setTheme(R.style.AppTheme);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_setting);
        btnSave = (Button)findViewById(R.id.bSave);
        myswitch=(Switch)findViewById(R.id.switch1);
        myswitch2 = (Switch)findViewById(R.id.switch2);
        if (sharedpref.loadNightModeState()==true) {
            myswitch.setChecked(true);
        }

        btnSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                        if(myswitch.isChecked()) {
                            sharedpref.setNightModeState(true);
                        }
                        else {
                            sharedpref.setNightModeState(false);
                        }
                        if (myswitch2.isChecked()) {
                            sharedpref.setTextSize(true);
                        }
                        else {
                            sharedpref.setTextSize(false);
                        }
                restartApp ();
            }
        });
    }

    public void restartApp () {
        Intent i = new Intent(getApplicationContext(),SettingActivity.class);
        startActivity(i);
        finish();
    }
}
