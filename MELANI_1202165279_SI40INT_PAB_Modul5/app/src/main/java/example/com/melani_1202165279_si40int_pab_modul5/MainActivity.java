package example.com.melani_1202165279_si40int_pab_modul5;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import java.util.LinkedList;

public class MainActivity extends AppCompatActivity {

    Button btnCreate;
    Button btnList;
    Button btnSetting;
    SharedPref sharedpref;
    TextView tv3;
    String day;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        sharedpref = new SharedPref(this);

        if(sharedpref.loadNightModeState()== true) {
            setTheme(R.style.darktheme);
            day = "Good Night";
        }
        else  {
            setTheme(R.style.AppTheme);
            day = "Good Morning";
        }
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        tv3 = findViewById(R.id.textView3);
        tv3.setText(day);
        btnCreate = findViewById(R.id.bBuat);
        btnList= findViewById(R.id.bList);
        btnSetting = findViewById(R.id.bSetting);

        btnCreate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(MainActivity.this,  CreateActivity.class));
            }
        });

        btnList.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(MainActivity.this,  ListActivity.class));
            }
        });

        btnSetting.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(MainActivity.this,  SettingActivity.class));
            }
        });


    }
}
