package example.com.melani_1202165279_si40int_pab_modul5;

import android.content.Context;
import android.content.SharedPreferences;

/**
 * Created by Aws on 21/12/2017.
 */

public class SharedPref {
    SharedPreferences mySharedPref ;
    public SharedPref(Context context) {
        mySharedPref = context.getSharedPreferences("filename", Context.MODE_PRIVATE);
    }
    // this method will save the nightMode State : True or False
    public void setNightModeState(Boolean state) {
        SharedPreferences.Editor editor = mySharedPref.edit();
        editor.putBoolean("NightMode",state);
        editor.commit();
    }
    // this method will load the Night Mode State
    public Boolean loadNightModeState (){
        Boolean state = mySharedPref.getBoolean("NightMode",false);
        if (state == null){
            return false;
        }

        else {
            return  state;
        }

    }

    public void setTextSize(Boolean state) {
        SharedPreferences.Editor editor = mySharedPref.edit();
        editor.putBoolean("TextSize", state);
        editor.commit();
    }

    public Boolean loadTextState (){
        Boolean state = mySharedPref.getBoolean("TextSize",false);
        if (state == null){
            return false;
        }

        else {
            return  state;
        }

    }
}
